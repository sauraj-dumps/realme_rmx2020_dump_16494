## aosp_RMX2020-eng 12 SP1A.210812.016 cb4a3381f1 test-keys
- Manufacturer: realme
- Platform: mt6768
- Codename: RMX2020
- Brand: realme
- Flavor: aosp_RMX2020-eng
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: cb4a3381f1
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: realme/aosp_RMX2020/RMX2020:12/SP1A.210812.016/cb4a3381f1:eng/test-keys
- OTA version: 
- Branch: aosp_RMX2020-eng-12-SP1A.210812.016-cb4a3381f1-test-keys
- Repo: realme_rmx2020_dump_16494


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
